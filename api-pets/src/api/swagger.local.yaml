---
swagger: "2.0"
info:
  title: "OCTO Academy - API Pets"
  description: "API de test pour la formation Sécuriser et Manager son API (APISM)."
  version: "1.0.0"
  contact:
    email: "woapi@octo.com"
  license:
    name: "Apache 2.0"
    url: "http://www.apache.org/licenses/LICENSE-2.0.html"
host: "localhost:8080"
basePath: "/v2"
tags:
- name: "pet"
  description: "Everything about your Pets"
  externalDocs:
    description: "Find out more"
    url: "http://swagger.io"
- name: "store"
  description: "Access to Petstore orders"
- name: "user"
  description: "Operations about user"
  externalDocs:
    description: "Find out more about our store"
    url: "http://swagger.io"
schemes:
- "http"
paths:
  /pets:
    post:
      tags:
      - "pet"
      summary: "Add a new pet to the store"
      description: ""
      operationId: "addPet"
      consumes:
      - "application/json"
      - "application/xml"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - in: "body"
        name: "body"
        description: "Pet object that needs to be added to the store"
        required: true
        schema:
          $ref: "#/definitions/Pet"
      responses:
        405:
          description: "Invalid input"
      #security:
      #- petstore_auth:
      #  - "write:pets"
      #  - "read:pets"
      x-swagger-router-controller: "Pet"
    get:
      tags:
      - "pet"
      summary: "Finds Pets by status"
      description: "Multiple status values can be provided with comma separated strings"
      operationId: "findPetsByStatus"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      #- name: "x-api-key"
      #  in: "header"
      #  required: false
      #  type: "string"
      - name: "status"
        in: "query"
        description: "Status values that need to be considered for filter"
        required: true
        type: "array"
        items:
          type: "string"
          default: "available"
          enum:
          - "available"
          - "pending"
          - "sold"
        collectionFormat: "multi"
      responses:
        200:
          description: "successful operation"
          schema:
            type: "array"
            items:
              $ref: "#/definitions/Pet"
        400:
          description: "Invalid status value"
        401:
          description: "Unauthorized"
      x-swagger-router-controller: "Pet"
  /pets/{petId}:
    get:
      tags:
      - "pet"
      summary: "Find pet by ID"
      description: "Returns a single pet"
      operationId: "getPetById"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "petId"
        in: "path"
        description: "ID of pet to return"
        required: true
        type: "integer"
        format: "int64"
      responses:
        200:
          description: "successful operation"
          schema:
            $ref: "#/definitions/Pet"
        400:
          description: "Invalid ID supplied"
        401:
          description: "Unauthorized"
        404:
          description: "Pet not found"
      x-swagger-router-controller: "Pet"
    put:
      tags:
      - "pet"
      summary: "Updates a pet in the store with form data"
      description: ""
      operationId: "updatePetWithForm"
      consumes:
      - "application/x-www-form-urlencoded"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "petId"
        in: "path"
        description: "ID of pet that needs to be updated"
        required: true
        type: "integer"
        format: "int64"
      - name: "name"
        in: "formData"
        description: "Updated name of the pet"
        required: false
        type: "string"
      - name: "status"
        in: "formData"
        description: "Updated status of the pet"
        required: false
        type: "string"
      responses:
        405:
          description: "Invalid input"
      #security:
      #- petstore_auth:
      #  - "write:pets"
      #  - "read:pets"
      x-swagger-router-controller: "Pet"
    delete:
      tags:
      - "pet"
      summary: "Deletes a pet"
      description: ""
      operationId: "deletePet"
      produces:
      - "application/xml"
      - "application/json"
      parameters:
      - name: "petId"
        in: "path"
        description: "Pet id to delete"
        required: true
        type: "integer"
        format: "int64"
      responses:
        400:
          description: "Invalid ID supplied"
        401:
          description: Unauthorized
        404:
          description: "Pet not found"
      #security:
      #- petstore_auth:
      #  - "write:pets"
      #  - "read:pets"
      x-swagger-router-controller: "Pet"
# securityDefinitions:
  #petstore_auth:
  #  type: "oauth2"
  #  authorizationUrl: "https://sabinda.eu.auth0.com/authorize"
  #  flow: "implicit"
  #  scopes:
  #    write:pets: "modify pets in your account"
  #    read:pets: "read your pets"
  #api_key:
  #  type: "apiKey"
  #  name: "x-api-key"
  #  in: "header"
definitions:
  Order:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      petId:
        type: "integer"
        format: "int64"
      quantity:
        type: "integer"
        format: "int32"
      shipDate:
        type: "string"
        format: "date-time"
      status:
        type: "string"
        description: "Order Status"
        enum:
        - "placed"
        - "approved"
        - "delivered"
      complete:
        type: "boolean"
        default: false
    example:
      petId: 6
      quantity: 1
      id: 0
      shipDate: "2000-01-23T04:56:07.000+00:00"
      complete: false
      status: "placed"
    xml:
      name: "Order"
  Category:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
    example:
      name: "name"
      id: 6
    xml:
      name: "Category"
  Tag:
    type: "object"
    properties:
      id:
        type: "integer"
        format: "int64"
      name:
        type: "string"
    example:
      name: "name"
      id: 1
    xml:
      name: "Tag"
  Pet:
    type: "object"
    required:
    - "name"
    - "photoUrls"
    properties:
      id:
        type: "integer"
        format: "int64"
      category:
        $ref: "#/definitions/Category"
      name:
        type: "string"
        example: "doggie"
      photoUrls:
        type: "array"
        xml:
          name: "photoUrl"
          wrapped: true
        items:
          type: "string"
      tags:
        type: "array"
        xml:
          name: "tag"
          wrapped: true
        items:
          $ref: "#/definitions/Tag"
      status:
        type: "string"
        description: "pet status in the store"
        enum:
        - "available"
        - "pending"
        - "sold"
    example:
      photoUrls:
      - "photoUrls"
      - "photoUrls"
      name: "doggie"
      id: 0
      category:
        name: "name"
        id: 6
      tags:
      - name: "name"
        id: 1
      - name: "name"
        id: 1
      status: "available"
    xml:
      name: "Pet"
  ApiResponse:
    type: "object"
    properties:
      code:
        type: "integer"
        format: "int32"
      type:
        type: "string"
      message:
        type: "string"
    example:
      code: 0
      type: "type"
      message: "message"
