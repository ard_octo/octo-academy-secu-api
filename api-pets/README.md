# API Animaux (Pets)

Ce serveur est généré par [swagger-codegen](https://github.com/swagger-api/swagger-codegen). Il permet de générer une API de Stub en suivant les spécifications d'[OpenAPI](https://github.com/OAI/OpenAPI-Specification).

## Tech

### Démarrer le serveur en local

Lancer cette commande :

```sh
npm start
```

### Démarrer le serveur avec Docker

Pour construire et démarrer le serveur (si le network Docker où se trouve Kong est appelé `kong-net` ; cf [README de la partie Kong](../infra/kong/README.md)).

```sh
docker build -t api-pets .
docker run -d --name api-pets -p 8081:8080 --network kong-net api-pets:latest
```

### Documentation Swagger

Pour voir la liste des endpoints de l'API et les essayer :

```sh
# Si démarré sans Docker
open http://localhost:8080/docs
# Si démarré avec Docker
open http://localhost:8081/docs
```

## Sécuriser l'API

### 1 - Utiliser une API-key custom

Ajouter le paramètre "x-api-key" dans le fichier Swagger `api-pets/src/api/swagger.local.yaml` (par exemple ligne 67 pour le `GET /pets`).

```yml
parameters:
  - name: "x-api-key"
    in: "header"
    required: false
    type: "string"
```

Mettre à jour le fichier `controllers/Pets.js` de la façon suivante (en définissant `123456` en tant que clé d'API) :

```js
module.exports.findPetsByStatus = function findPetsByStatus (req, res, next) {

 if(req.headers['x-api-key'] != "123456") {
    res.writeHead(401, {'Content-Type': 'application/json'});
    res.end('{"error": 401, "Message": "Unauthorized"}');
    return;
  }
```

### 2 - Utiliser une API-key avec le plugin Kong

Revenir en arrière sur les ajouts pour l'API-key custom et aller consulter le [README de la partie Kong](../infra/kong/README.md) pour configurer le plugin.

Mettre à jour la clé `paths` dans la configuration Swagger ( `api-pets/src/api/swagger.local.yaml`) :

```yml
paths:
  /pets:
    post:
      security:
      - apiKey

securityDefinitions:
  api_key:
    type: "apiKey"
    name: "apikey"
    in: "header"
```

### 3 - Sécuriser l'API avec les plugins Oauth2 de Kong

Lire les instructions Oauth2 dans le [README de la partie Kong](../infra/kong/README.md).

Mettre à jour la clé `paths` dans la configuration Swagger ( `api-pets/src/api/swagger.local.yaml`) :

```yml
paths:
  /pets:
    post:
      security:
      - petstore_auth:
        - "write:pets"
        - "read:pets"

securityDefinitions:
    petstore_auth:
         type: "oauth2"
         authorizationUrl: "<authorize endpoint here>"
         flow: "implicit"
         scopes:
           write:pets: "modify pets in your account"
           read:pets: "read your pets"
```

