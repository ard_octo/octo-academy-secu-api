# Octo Academy - Formation Sécuriser et Manager son API (APISM)

Ce repository sert de support à la formation Sécuriser et Manager son API donnée par Octo Academy (OCTO Technology).

Il s'agit de découvrir Kong, solution d'API Management, qui embarque des fonctionnalités de sécurisation API.

## Documentations

Le projet contient deux parties
- Une API "Pets" créée avec `swagger-codegen` : [README api-pets](./api-pets/README.md)
- Un serveur d'API Management qui utilise `Kong` : [README Kong](./infra/kong/README.md)

## Structure du repository

```
.
├── README.md
├── api-pets
│   ├── Dockerfile
│   ├── README.md
│   ├── package-lock.json
│   ├── package.json
│   └── src
└── infra
    └── kong
        ├── Dockerfile
        ├── README.md
        ├── kong.conf
        └── oauth2-login
```